const formulario = document.querySelector('#formulario');
const listaTweets = document.querySelector('#lista-tweets');

let tweets = [];

document.addEventListener('DOMContentLoaded', main);

function main() {
  eventListeners();
    tweets = JSON.parse(localStorage.getItem('tweets')) || [];
    crearHTML();
}

function eventListeners() {
  formulario.addEventListener('submit', agregarTweet);
}
function agregarTweet(e) {
  e.preventDefault();
  const tweet = document.querySelector('#tweet');
  if (tweet.value.trim() === '') {
    mostrarError('El mensaje no puede ir vacio');
    return;
  }
  const tweetObj = {
    id: Date.now(),
    tweet: tweet.value.trim()
  }
  tweets = [...tweets, tweetObj];
  crearHTML();
  formulario.reset();
  tweet.focus();
}
function mostrarError(texto) {
  const mensaje = document.createElement('p');
  mensaje.textContent = texto;
  mensaje.classList.add('error');
  const contenido = document.querySelector('#contenido');
  contenido.appendChild(mensaje);
  setTimeout(() => {
    mensaje.remove();
  }, 2000);
}
function crearHTML() {
  limpiarHTML();
  if (tweets.length > 0 ) {
    tweets.forEach(tweet => {
      const boton = document.createElement('a');
      boton.textContent = 'x';
      boton.classList.add('borrar-tweet');
      boton.onclick = () => {
        borrarTweet(tweet.id);
      }

      const li = document.createElement('li');
      li.innerHTML = tweet.tweet;
      li.appendChild(boton);
      listaTweets.appendChild(li);
    });
  }
  sincronizarStorage();
}
function limpiarHTML() {
  while (listaTweets.firstChild) {
    listaTweets.removeChild(listaTweets.firstChild);
  }
}
function sincronizarStorage() {
  localStorage.setItem('tweets', JSON.stringify(tweets));
}
function borrarTweet(id) {
  tweets = tweets.filter(tweet => tweet.id !== id);
  crearHTML();
}